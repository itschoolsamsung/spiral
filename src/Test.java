public class Test {
    public static void printArr(int[][] arr) {
        for (int row[] : arr) {
            for (int a : row) {
                System.out.printf("%3d", a);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int[][] arr = new int[5][5];
        int x = 4, y = 0, num = 24;
        String direction = "down";

        do {
            arr[y][x] = num;
            switch (direction) {
                case "left":
                    if (x > 0 && arr[y][x-1] == 0) {
                        num--;
                        x--;
                    } else {
                        direction = "up";
                    }
                    break;
                case "up":
                    if (y > 0 && arr[y-1][x] == 0) {
                        num--;
                        y--;
                    } else {
                        direction = "right";
                    }
                    break;
                case "right":
                    if (x < 4 && arr[y][x+1] == 0) {
                        num--;
                        x++;
                    } else {
                        direction = "down";
                    }
                    break;
                case "down":
                    if (y < 4 && arr[y+1][x] == 0) {
                        num--;
                        y++;
                    } else {
                        direction = "left";
                    }
                    break;
            }
        } while (num > 0);

        printArr(arr);
    }
}
